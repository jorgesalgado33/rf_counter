/**
 * @file main.cpp
 * @author Jorge Salgado (jorgesalgado23@gmail.com)
 * @brief GAME PONG with ESP32 and TFT ILI9341
 * @version 
 * @date 01-01-2021
 * 
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDES - ARDUINO
 *******************************************************************************************************************************************/
#include <Arduino.h>
#include "../lib//Definition/GlobalDef.h"
#include <Adafruit_STMPE610.h>
/*******************************************************************************************************************************************
 *  												INCLUDES - 
 *******************************************************************************************************************************************/

// This optional setting causes Encoder to use more optimized code,
// It must be defined before Encoder.h is included.
#include "SPI.h"
#include "Wire.h"
#include "Adafruit_ILI9341.h"
#include "TouchScreen.h"
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

#define SIZE_FILTER 6
#define SIZE_PLAYER 50
#define SIZE_JUMP 10
#define RATIO_BALL 5
#define NRST_RF 22

/*Touch screem PINs*/
#define TS_IRQ 33
#define TS_DO 25
#define TS_DI 26
#define TS_CS 27
#define TS_CLK 14


/*Screem PINs*/
#define SC_CS 15
#define SC_DC 4
#define SC_MOSI 5
#define SC_SCLK 18
#define SC_RST 2
#define SC_MISO 21
#define LED_BUILTIN 19


#define UP_PLAYER 1
#define DOWN_PLAYER 0

unsigned long encoder2lastToggled;
bool encoder2Paused = false;
Adafruit_ILI9341 *screem_tft;
Adafruit_STMPE610 *touchSc;
// Task declaration
TaskHandle_t TaskCore0, TaskCore1;

// Timer declaration
hw_timer_t * timer0 = NULL;
hw_timer_t * timer1 = NULL;
hw_timer_t * timer2 = NULL;
hw_timer_t * timer3 = NULL;
portMUX_TYPE timerMux0 = portMUX_INITIALIZER_UNLOCKED;
portMUX_TYPE timerMux1 = portMUX_INITIALIZER_UNLOCKED;
portMUX_TYPE timerMux2 = portMUX_INITIALIZER_UNLOCKED;
portMUX_TYPE timerMux3 = portMUX_INITIALIZER_UNLOCKED;

// Timer Flags
bool flagTimer0 = false;
bool flagTimer1 = false;
bool flagTimer2 = false;
bool flagTimer3 = false;

uint8_t player1 = 2;
uint8_t player2 = 2;
uint16_t CounterDisplay = 0;
uint16_t Prev_CounterDisplay = 1;
uint8_t player2Points = 0;

uint8_t temp = UP_PLAYER;
uint8_t temp2 = UP_PLAYER;
/*******************************************************************************************************************************************
 *  												TEST
 *******************************************************************************************************************************************/
uint32_t test_counter = 0;


void InitRF();

/*******************************************************************************************************************************************
 *  												CORE LOOPS
 *******************************************************************************************************************************************/
// Loop of core 0
void LoopCore0( void * parameter ){
    while(true) {
        // Code for Timer 1 interruption
        if (flagTimer1){
            flagTimer1 = true;

        }



        // Delay to feed WDT
        delay(10);
    }
}

// Loop of core 1
void LoopCore1( void * parameter ){

    while(true) {
        /* Wait RF signal Lora*/
        if(Serial2.available()){
                /*Read Serial Data*/
                String incomingstring = Serial2.readString();
                
                if(incomingstring.indexOf("H") > 0)
                {
                    CounterDisplay +=1;
                }
                else{
                    /*Nothing to do*/
                }
            }
 
        screem_tft->setCursor(140,80);
        if( Prev_CounterDisplay != CounterDisplay )
        {
            /*erase previous Number*/
            screem_tft->fillRoundRect(100,60,150,60,1,ILI9341_BLACK);
        }
        else{
            screem_tft->printf("%d", CounterDisplay);
        }
        Prev_CounterDisplay = CounterDisplay;
        
        delay(100);  
    }
}

/*******************************************************************************************************************************************
 *  												TIMERS
 *******************************************************************************************************************************************/
// TIMER 0
void IRAM_ATTR onTimer0(){
    portENTER_CRITICAL_ISR(&timerMux0);

    flagTimer0 = true;

    portEXIT_CRITICAL_ISR(&timerMux0);
}

// TIMER 1
void IRAM_ATTR onTimer1(){
    portENTER_CRITICAL_ISR(&timerMux1);

    flagTimer1 = true;

    portEXIT_CRITICAL_ISR(&timerMux1);
}

// TIMER 2
void IRAM_ATTR onTimer2(){
    portENTER_CRITICAL_ISR(&timerMux2);

    flagTimer2 = true;

    portEXIT_CRITICAL_ISR(&timerMux2);
}

// TIMER 3
void IRAM_ATTR onTimer3(){
    portENTER_CRITICAL_ISR(&timerMux3);

    flagTimer3 = true;

    portEXIT_CRITICAL_ISR(&timerMux3);
}

/*******************************************************************************************************************************************
 *  												SETUP
 *******************************************************************************************************************************************/
void setup() {

    // Disable brownout detector
    //WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); 
    //rtc_wdt_protect_off();
    //rtc_wdt_disable();

    // Serial Port
    Serial.begin(115200);
    Serial2.begin(115200);
    //Serial.println(" +++++ ESP32 TESTING +++++");
    
    /*************************************************************************************************/
    /*************************************************************************************************/
    screem_tft = new Adafruit_ILI9341(SC_CS,SC_DC,SC_MOSI,SC_SCLK,SC_RST,SC_MISO);
    screem_tft->begin();

    pinMode(LED_BUILTIN, OUTPUT);    
    
    pinMode(NRST_RF,OUTPUT);
    
    digitalWrite(LED_BUILTIN,HIGH);
    digitalWrite(NRST_RF,LOW);


    InitRF();
    /*****************************************************************************************/
 
    /*****************************************************************************************/
    

    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /************************************************************************************************/
   
    // Task of core 0
    xTaskCreatePinnedToCore(
        LoopCore0,  /* Function to implement the task */
        "LoopCore0",    /* Name of the task */
        10000,      /* Stack size in words */
        NULL,       /* Task input parameter */
        1,          /* Priority of the task */
        &TaskCore0, /* Task handle. */
        0);         /* Core where the task should run */

    delay(500);  // needed to start-up task1

    // Task of core 1
    xTaskCreatePinnedToCore(
        LoopCore1,  /* Function to implement the task */
        "LoopCore1",    /* Name of the task */
        10000,      /* Stack size in words */
        NULL,       /* Task input parameter */
        1,          /* Priority of the task */
        &TaskCore1, /* Task handle. */
        1);         /* Core where the task should run */

    // Timer0
    Serial.println("start timer 0");
    timer0 = timerBegin(0, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer0, &onTimer0, true); // edge (not level) triggered 
    timerAlarmWrite(timer0, 1000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Timer1
    Serial.println("start timer 1");
    timer1 = timerBegin(1, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer1, &onTimer1, true); // edge (not level) triggered 
    timerAlarmWrite(timer1, 100000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Timer2
    Serial.println("start timer 2");
    timer2 = timerBegin(2, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer2, &onTimer2, true); // edge (not level) triggered 
    timerAlarmWrite(timer2, 1000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Timer3
    Serial.println("start timer 3");
    timer3 = timerBegin(3, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer3, &onTimer3, true); // edge (not level) triggered 
    timerAlarmWrite(timer3, 1000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Enable the timer alarms
    timerAlarmEnable(timer0); // enable
    timerAlarmEnable(timer1); // enable
    //timerAlarmEnable(timer2); // enable
    //timerAlarmEnable(timer3); // enable
}

/*******************************************************************************************************************************************
 *  												Main Loop
 *******************************************************************************************************************************************/
// Loop not used
void loop() {
    vTaskDelete(NULL);
}

void InitRF(){

    player1 =(ILI9341_TFTWIDTH/2)-(SIZE_PLAYER/2);
    player2 =(ILI9341_TFTWIDTH/2)-(SIZE_PLAYER/2);

    screem_tft->setRotation(1);
    screem_tft->fillScreen(ILI9341_BLACK);
    screem_tft->setTextSize(3);
    screem_tft->setCursor(70,30);
    screem_tft->print("Counter :");
    screem_tft->setCursor((ILI9341_TFTHEIGHT/2)-120,ILI9341_TFTWIDTH-50);
    screem_tft->print("Jorge Salgado");
}



